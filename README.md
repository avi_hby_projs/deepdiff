# DeepDiff

This tool is used to get a diff between two properties and xml files. Instead of line by line compare, this tool does a key-value diff to get an appropriate difference based on the type of file.